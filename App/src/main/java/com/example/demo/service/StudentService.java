package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.demo.pojo.*;
import com.example.demo.repository.StudentRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service  
public class StudentService {  
    @Autowired  
    private StudentRepo repo;  
    public List<StudentPOJO> getAllUsers(){  
        List<StudentPOJO>userRecords = new ArrayList<>();  
        repo.findAll().forEach(userRecords::add);  
        return userRecords;  
    }  
    public Optional<StudentPOJO> getUser(String id){  
        return repo.findById(id); 
    }  
    public void addUser(StudentPOJO userRecord){  
        repo.save(userRecord);  
    }  
    public void delete(String id){  
        repo.deleteById(id);  
    }  
}  
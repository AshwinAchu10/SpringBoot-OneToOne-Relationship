package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.pojo.StudentPOJO;  
public interface StudentRepo extends CrudRepository<StudentPOJO, String> {  
}
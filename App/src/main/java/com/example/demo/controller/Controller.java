package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.pojo.StudentPOJO;
import com.example.demo.service.StudentService;
import java.util.List;  


@RestController  
public class Controller {  
    @Autowired  
    private StudentService stuService;   
    @RequestMapping("/")  
    public List<StudentPOJO> getAllUser(){  
        return stuService.getAllUsers();  
    }     
    @RequestMapping(value="/add-user", method=RequestMethod.POST)  
    public void addUser(@RequestBody StudentPOJO id){  
        stuService.addUser(id);  
    }  
    @RequestMapping(value="/user/{id}", method=RequestMethod.GET)  
    public Optional<StudentPOJO> getUser(@PathVariable String id){  
        return stuService.getUser(id);  
    }  
}  
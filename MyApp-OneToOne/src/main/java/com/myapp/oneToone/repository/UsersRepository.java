package com.myapp.oneToone.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.myapp.oneToone.pojo.Users;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Integer> {

	Optional<Users> findById(long id);

	
	

	}

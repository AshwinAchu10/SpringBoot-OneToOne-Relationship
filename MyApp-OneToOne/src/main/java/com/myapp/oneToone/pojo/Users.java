package com.myapp.oneToone.pojo;

import javax.persistence.*;

@Entity
public class Users {
    private int id;
    private String name;
    private int age;
    private UserDetail userDetail;

    public Users(){

    }

    public Users(String name){
        this.name = name;
    }

    public Users(String name, UserDetail userDetail){
        this.name = name;
        this.userDetail = userDetail;
    }


    public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_detail_id")
    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    @Override
    public String toString() {
        return String.format(
                "User[id=%d, name='%s', Age='%d']",
                id, name, userDetail.getPhNum());
    }
}
package com.myapp.oneToone.pojo;
import javax.persistence.*;

@Entity
@Table(name = "user_detail")
public class UserDetail {
    private Integer id;
    private Integer phNum;
    private Users users;

    public UserDetail(){

    }

    public UserDetail(Integer phNum){
        this.phNum = phNum;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "phNum")
    public Integer getPhNum() {
        return phNum;
    }

    public void setPhNum(Integer phNum) {
        this.phNum = phNum;
    }

    @OneToOne(mappedBy = "userDetail")
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
}
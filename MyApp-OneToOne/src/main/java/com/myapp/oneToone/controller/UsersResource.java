package com.myapp.oneToone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.oneToone.pojo.Users;
import com.myapp.oneToone.repository.UsersRepository;
import com.myapp.oneToone.service.Service;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/users")
public class UsersResource {
   
    @Autowired
    private Service service;
    
    
    @RequestMapping(value="/all", method=RequestMethod.GET)
    public List<Users> getUsersContact() {
        return service.findAll();
    }

    
    @RequestMapping(value="/add", method=RequestMethod.POST)  
    public void Add(@RequestBody Users users){  
        service.addDetail(users);  
    }  
    
    @RequestMapping(value="/id/{id}",method=RequestMethod.GET)
    public Optional<Users> getId(@PathVariable("id")  long id) {
        return service.findById(id);
    }

   
}

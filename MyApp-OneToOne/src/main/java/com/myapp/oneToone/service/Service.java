package com.myapp.oneToone.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.myapp.oneToone.pojo.Users;
import com.myapp.oneToone.repository.UsersRepository;

@org.springframework.stereotype.Service

public class Service {  
    @Autowired  
    private UsersRepository rep;

		public  Optional<Users>  findById(long id) {
		return this.rep.findById(id);
	}

	
	public void addDetail(Users users) {
		rep.save(users);
		
	}

	public List<Users> findAll() {
		return this.rep.findAll();
		}
	

 
}  